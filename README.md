# Semper GraphQL Backend

## run server locally

These commands will install the JavaScript libraries and run the Apollo Studio on http://localhost:4000. It is recommended to use `nvm` – the [Node Version Manager](https://github.com/nvm-sh/nvm) – to use a specific node version, e.g. `nvm use v19.5.0`

```bash
npm install
node index.js
```

Or run the server in `DEBUG` mode, so you see all the created Cypher statements:

```bash
DEBUG=@neo4j/graphql:* node index.js
```

You can stop the server with `CTRL-c`.

## run development server in container

The command below will build the image (`--build`), run the container (`up`) and daemonize (`-d`) the process. The command needs to be issued in the root directory of this project.

- You can omit the `--build` if you have not changed anything in the Dockerfile
- You can omit the `-d` to watch what's happening on and be able to stop the container with a simple `CTRL-c`

```
docker-compose up --build -d
```

Stop the container with this command:

```
docker-compose stop
```

## run productive server in container

To freshly build the container and run it daemonized:

```
docker-compose -f docker-compose-prod.yml up --build -d
```

To stop the container:

```
docker-compose -f docker-compose-prod.yml stop
```
