import { ApolloServer } from "@apollo/server";
import { startStandaloneServer } from "@apollo/server/standalone";
import { Neo4jGraphQL } from "@neo4j/graphql";
import neo4j from "neo4j-driver";
import { ApolloServerPluginLandingPageGraphQLPlayground } from "@apollo/server-plugin-landing-page-graphql-playground";

const typeDefs = `#graphql
    
    type tei @plural(value: "teis") @node(labels: ["TEI"]) @mutation(operations: []) {
        filename: String
        idno: String
    }
    type graphic @mutation(operations: []) @query(read: false, aggregate: false) {
        filename: String
        idno: String
        height: String
        width: String
        image: String @alias(property: "url")
        surface: [surface!]! @relationship(type: "CONTAINS", direction: IN, aggregate: false)
    }
    type surface @mutation(operations: []) @query(read: false, aggregate: false) {
        filename: String
        idno: String
        lrx: Int
        lry: Int
        ulx: Int
        uly: Int
        graphic: graphic @relationship(type: "CONTAINS", direction: OUT, aggregate: false)
    }
    type zone @mutation(operations: []) @query(read: false, aggregate: false) {
        filename: String
        idno: String
        points: String
        rendition: String
        xmlId: String @alias(property: "xml:id")
        surface: surface! @relationship(type: "CONTAINS", direction: IN, aggregate: false)
    }
    type token @plural(value: "tokens") @node(labels: ["token"]) @mutation(operations: []) @query(read: false, aggregate: false) {
        string: String
        whitespace: String
        pos: String
        tag: String
        endsWithHyphen: Boolean @alias(property: "ends_with_hyphen")
        filename: String
        xmlId: String @alias(property: "xml:id")
        labels: [String] @cypher(statement: "RETURN LABELS(this)")
    }
    type paragraph @plural(value: "paragraphs") @node(labels: ["p"]) @mutation(operations: []) {
        xmlId: String @alias(property: "xml:id")
        prev: String
        next: String
        facs: String
        idno: String
        filename: String
        tokens: [token!]! @cypher(
            statement: """
            MATCH (this)-[:NEXT]->(t),
                  (this)-[:LAST]->(lasttoken),
                   textpath = shortestPath((t)-[:NEXT|CONCATENATED*]->(lasttoken))
            WHERE NOT ID(t) = ID(lasttoken)
            UNWIND nodes(textpath) as token
            RETURN token
            """
        )
        references: [reference] @cypher(statement: "MATCH (r :rs) WHERE r.filename=this.filename and r.paragraph_id=this.\`xml:id\` RETURN r")
        referenceCount: Int @cypher(statement: "MATCH (r :rs) WHERE r.filename=this.filename and r.paragraph_id=this.\`xml:id\` RETURN COUNT(r)")
        text: String @cypher(
            statement: """
            MATCH (this)-[:NEXT]->(t),
                  (this)-[:LAST]->(lasttoken),
                   textpath = shortestPath((t)-[:NEXT|CONCATENATED*]->(lasttoken))
            WHERE NOT ID(t) = ID(lasttoken)
            OPTIONAL MATCH (p2 :p {prev: this.filename+"#" + this.\`xml:id\`})-[:NEXT]->(t2),
                           (p2)-[:LAST]->(lasttoken2),
                   textpath2 = shortestPath((t2)-[:NEXT|CONCATENATED*]->(lasttoken2))
            WITH apoc.text.join(
                [
                    token in nodes(textpath)
                    WHERE NOT token:del 
                    | coalesce(token.string, "") + coalesce(token.whitespace," ")
                ],
                ""
            ) AS text1,
            apoc.text.join(
                [
                    token in nodes(textpath2)
                    WHERE NOT token:del 
                    | coalesce(token.string, "") + coalesce(token.whitespace," ")
                ],
                ""
            ) AS text2
            RETURN apoc.text.join([text1, coalesce(text2, "")],"") as text
            """
        ) 
        zone: zone @cypher(statement: "MATCH (z :zone) WHERE z.filename=this.filename AND z.\`xml:id\`=replace(this.facs, '#', '') RETURN z LIMIT 1")
        tei: tei @cypher(statement: "MATCH (t :TEI) WHERE t.filename=this.filename RETURN t")
        similar: [paragraph!]! @relationship(type: "IS_SIMILAR_TO", direction: OUT)
    }
    type reference @plural(value: "references") @node(labels: ["rs"]) @mutation(operations: []) {
        xmlId: String @alias(property: "ref")
        filename: String
        type: String
        idno: String
        tocFile: tocFile @cypher(statement: "MATCH (f :FILE) WHERE f.file=this.filename RETURN f")
        organisations: [organisation] @cypher(statement: "MATCH (o :organisation) WHERE o.\`xml:id\`=this.ref RETURN o") 
        persons: [person] @cypher(statement: "MATCH (p :person) WHERE p.\`xml:id\`=this.ref RETURN p") 
        artefacts: [artefact] @cypher(statement: "MATCH (a :artefact) WHERE a.\`xml:id\`=this.ref RETURN a") 
        concepts: [concept] @cypher(statement: "MATCH (c :concept) WHERE c.\`xml:id\`=this.ref RETURN c") 
        peoples: [people] @cypher(statement: "MATCH (p :people) WHERE p.\`xml:id\`=this.ref RETURN p") 
        bibliographies: [bibliography] @cypher(statement: "MATCH (b :bibliography) WHERE b.\`xml:id\`=this.ref RETURN b") 
        paragraphId: String @alias(property: "paragraph_id")
        paragraph: paragraph @cypher(statement: "MATCH (p :p) WHERE p.filename=this.filename AND p.\`xml:id\`=this.paragraph_id RETURN p")
    }
    type sari @plural(value: "saris") @node(labels: ["ns0__E42_Identifier"]) @mutation(operations: []) {
        xmlId: String @alias(property: "ns0__P190_has_symbolic_content")
        sariUri: String @alias(property: "uri")
        externalUris: [String] @cypher(statement: "MATCH (this)<-[:ns0__P1_is_identified_by]-()-[:\`ns5__L54_is_same-as\`]->(r :Resource) RETURN COLLECT(DISTINCT(r.uri)) as uris")
    }
    type organisation @mutation(operations: []) {
        name: [String] @alias(property: "orgName")
        type: String
        locationCountry: String @alias(property: "location:country")
        locationSettlement: String @alias(property: "location:settlement")
        xmlId: String @alias(property: "xml:id")
        references: [reference!]! @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN r")
        referenceCount: Int @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN COUNT(r)")
        description: description @cypher(statement: "MATCH (:body)--(p :p) WHERE p.idno=this.\`xml:id\` RETURN p")
    }
    type place @mutation(operations: []) {
        name: String @alias(property: "placeName")
        type: String
        region: String
        country: String
        xmlId: String @alias(property: "xml:id")
        references: [reference!]! @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN r")
        referenceCount: Int @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN COUNT(r)")
        description: description @cypher(statement: "MATCH (:body)--(p :p) WHERE p.idno=this.\`xml:id\` RETURN p")
    }
    type description @mutation(operations: []) {
        tokens: [token!]! @cypher(
            statement: """
            MATCH (this)-[:NEXT]->(t),
                  (this)-[:LAST]->(lasttoken),
                   textpath = shortestPath((t)-[:NEXT|CONCATENATED*]->(lasttoken))
            WHERE NOT ID(t) = ID(lasttoken)
            UNWIND nodes(textpath) as token
            RETURN token
            """
        )
        text: String @cypher(
            statement: """
            MATCH (this)-[:NEXT]->(t),
                  (this)-[:LAST]->(lasttoken),
                   textpath = shortestPath((t)-[:NEXT|CONCATENATED*]->(lasttoken))
            WHERE NOT ID(t) = ID(lasttoken)
            RETURN apoc.text.join(
                [
                    token in nodes(textpath)
                    WHERE NOT token:del 
                    | coalesce(token.string, "") + coalesce(token.whitespace," ")
                ],
                ""
            ) as text
            """
        )
        responsible: [String]! @cypher(
            statement: """
            MATCH (:respStmt)-[CONTAINS]->(n :name)
            WHERE n.filename=this.filename
            RETURN COLLECT(DISTINCT(n.string)) as responsible
            """
        )
    }
    type person @plural(value: "persons") @mutation(operations: []) {
        name: String @alias(property: "persName")
        role: String
        birthDate: String @alias(property: "birth:date")
        birthPlace: String @alias(property: "birth:place")
        deathDate: String @alias(property: "death:date")
        deathPlace: String @alias(property: "death:place")
        xmlId: String @alias(property: "xml:id")
        references: [reference!]! @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN r")
        referenceCount: Int @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN COUNT(r)")
        description: description @cypher(statement: "MATCH (:body)-[:FIRST_ELEMENT]->(p :p) WHERE p.idno=this.\`xml:id\` RETURN p")
        sari: sari @cypher(statement: "MATCH (sari :ns0__E42_Identifier) WHERE sari.ns0__P190_has_symbolic_content=this.\`xml:id\` RETURN sari")
    }
    type artefact @mutation(operations: []) {
        name: [String] @alias(property: "artefactName")
        type: String @alias(property: "sortKey")
        xmlId: String @alias(property: "xml:id")
        references: [reference!]! @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN r")
        referenceCount: Int @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN COUNT(r)")
        description: description @cypher(statement: "MATCH (:body)-[:FIRST_ELEMENT]->(p :p) WHERE p.idno=this.\`xml:id\` RETURN p")
    }
    type people @plural(value: "peoples") @mutation(operations: []) {
        name: [String] @alias(property: "peopleName")
        xmlId: String @alias(property: "xml:id")
        references: [reference!]! @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN r")
        referenceCount: Int @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN COUNT(r)")
        description: description @cypher(statement: "MATCH (:body)--(p :p) WHERE p.idno=this.\`xml:id\` RETURN p")
    }
    type author @node(labels: ["author","bibliography", "index"]) @mutation(operations: []) {
        firstname: String @alias(property: "forename")
        lastname: String @alias(property: "surname")
        orgName: String
    }
    type bibliography @node(labels: ["bibliography","title", "index"]) @mutation(operations: []) {
        title: String
        xmlId: String @alias(property: "xml:id")
        authors: [author!]! @relationship(type: "HAS", direction: OUT)
        references: [reference!]! @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN r")
        referenceCount: Int @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN COUNT(r)")
        description: description @cypher(statement: "MATCH (:body)--(p :p) WHERE p.idno=this.\`xml:id\` RETURN p")
    }
    type concept @mutation(operations: []) {
        name: [String] @alias(property: "conceptName")
        xmlId: String @alias(property: "xml:id")
        references: [reference!]! @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN r")
        referenceCount: Int @cypher(statement: "MATCH (r :rs) WHERE r.ref=this.\`xml:id\` RETURN COUNT(r)")
        description: description @cypher(statement: "MATCH (:body)--(p :p) WHERE p.idno=this.\`xml:id\` RETURN p")
    }

    type tocTitle @exclude(operations: [READ, CREATE, UPDATE, DELETE]){
        language: String
        title: String
    }

    type toc @exclude(operations: [READ, CREATE, UPDATE, DELETE]){
        xmlId: String @alias(property: "xml:id")
        titles: [tocTitle]
    }
    type tocLocusLabel @exclude(operations: [READ, CREATE, UPDATE, DELETE]) {
        titles: [toc]
        label: String
        locus: String
    }
    type linkLocusLabel @exclude(operations: [READ, CREATE, UPDATE, DELETE]) {
        link: String
        label: String
        locus: String
    }
    type tocFile @node(labels: ["FILE", "TOC"]) @mutation(operations: []) {
        link: String @cypher(statement: "RETURN '/workspace/facsimile/semper/' + this.collection + '/pages/' + this.file + '/with/transcript'")
        label: String
        locus: String
        image: String
        thumbnail: String @cypher(statement: "RETURN replace(this.image,'/info.json', '/full/,150/0/default.jpg')")
        categorization: [bilingual] @cypher(statement: """
            MATCH paths=(this)<-[:HAS_FILE|HAS_CONTENT*]-()<-[:HAS_CONTENT]-(root :TOC {\`xml:id\`: 'stil'})
	        UNWIND paths as path
            CALL {
        	    WITH path
                UNWIND NODES(path) as cat
                CALL {
            	    WITH cat
                    MATCH titles = (cat)-[:HAS_TITLE]->(t)
                    RETURN apoc.map.fromLists(
                        collect(t.language),
                        collect(t.title)
                    ) as titles
                }
                RETURN titles
            }
		    RETURN titles
        """)
        collection: String
        depth: Int
        no: Int
        first: String
        last: String
        next: String
        prev: String
    }
    type bilingual {
        de: String
        en: String
    }

    type Query {
        descriptionForIdno(idno: String): [description] @cypher(
            statement: "MATCH (:body)-[:FIRST_ELEMENT]->(p :p) WHERE p.idno=$idno RETURN p"
        )
        artefactsThatStartWith(character: String, options: artefactOptions): [artefact] @cypher(
            statement: """
            MATCH (a: artefact)
            WHERE ANY(name in a.artefactName WHERE name =~ \\"^(?i)\\" + $character + \\".*\\" )
            RETURN a
            """
        )
        conceptsThatStartWith(character: String): [concept] @cypher(
            statement: """
            MATCH (c: concept)
            WHERE ANY(name in c.conceptName WHERE name =~ \\"^(?i)\\" + $character + \\".*\\" )
            RETURN c
            """
        )
        peoplesThatStartWith(character: String): [people] @cypher(
            statement: """
            MATCH (p: people)
            WHERE ANY(name in p.peopleName WHERE name =~ \\"^(?i)\\" + $character + \\".*\\" )
            RETURN p
            """
        )
        organisationsThatStartWith(character: String): [organisation] @cypher(
            statement: """
            MATCH (o: organisation)
            WHERE ANY(name in o.orgName WHERE name =~ \\"^(?i)\\" + $character + \\".*\\" )
            RETURN o
            """
        )
        tocTitlesForFilename(filename: String): [tocLocusLabel] @cypher(
            statement: """
            MATCH paths=(n :FILE {file:$filename})<-[:HAS_FILE|HAS_CONTENT*]-()<-[:HAS_CONTENT]-(root :TOC {\`xml:id\`: 'stil'})
                OPTIONAL MATCH (lab :label {filename: $filename})
                        WHERE lab.string IS NOT NULL
                OPTIONAL MATCH (loc :locus {filename: $filename})
                        WHERE loc.string IS NOT NULL
            UNWIND paths as path

            CALL {
                WITH path
                UNWIND NODES(path) as cat
                CALL {
                    WITH cat
                    MATCH titles = (cat)-[:HAS_TITLE]->(t)

                    WITH collect(
                        // create key-value from 2-element array
                        apoc.map.fromPairs(
                            [
                                // extract language and title from node as 2-element array
                                apoc.map.mget(t, ["language","title"])
                            ]
                        )
                    ) AS categorization
                    // skip empty arrays
                    WHERE size(categorization) > 0
                    RETURN categorization
                }

                // merge two key-value pairs (de and en)
                WITH collect(apoc.map.mergeList(categorization)) AS all_cat_titles
                RETURN all_cat_titles
            }
            RETURN reverse(all_cat_titles) as categorization, lab.string as label, loc.string as locus
            """
        )
    }
`;

const NEO4J_URI = "bolt://" + (process.env.NEO4J_HOST || "localhost") + ":" + (process.env.NEO4J_PORT || 7687);
console.log(NEO4J_URI);

const driver = neo4j.driver(
  NEO4J_URI,
  neo4j.auth.basic(process.env.NEO4J_USERNAME || "neo4j", process.env.NEO4J_PASSWORD || "neo4j")
);

const neoSchema = new Neo4jGraphQL({ typeDefs, driver });

const plugins = process.env.LOCAL_PLAYGROUND ? [ApolloServerPluginLandingPageGraphQLPlayground()] : [];

const server = new ApolloServer({
  plugins: plugins,
  schema: await neoSchema.getSchema(),
});

const { url } = await startStandaloneServer(server, {
  context: async ({ req }) => ({ req }),
  listen: { port: 4000 },
});

console.log(`🚀 Server ready at ${url}`);
