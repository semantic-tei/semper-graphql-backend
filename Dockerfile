FROM node:20-alpine3.17 AS base

WORKDIR /usr/app
COPY package*.json ./
EXPOSE 4000

FROM base AS production
ENV NODE_ENV=production
RUN npm config set proxy=http://proxy.ethz.ch:3128/
RUN npm ci
COPY . ./

FROM base AS dev
ENV NODE_ENV=development
RUN npm config set proxy=http://proxy.ethz.ch:3128/
RUN npm install -g nodemon
RUN npm install -g npm@9.7.2
RUN npm install
COPY . ./
